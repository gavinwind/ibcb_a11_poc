/**
 * @Copyright ®2017 Sinosoft Co. Ltd. All rights reserved.
 * 项目名称 : 统一支付平台
 * 创建日期 : 2017-11-22
 * 修改历史 :
 * 1. [2017-11-22]创建文件 by chenxin
 */
package com.sinosoft.poc.icbca11.product;

/**
 * 【类或接口功能描述】
 * @author chenxin
 * @date 17/11/22 下午2:43 
 * @version V1.0
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configuration注解　加载此类配置
 * EnableSwagger2注解　启用Swagger2
 */
@Configuration
@EnableSwagger2
public class PocProductSwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())	//创建API基本信息
                .groupName("")	//指定分组，对应(/v2/api-docs?group=)
                .pathMapping("")	//base地址，最终会拼接Controller中的地址
                .select()		//控制要暴露的接口
                .apis(RequestHandlerSelectors.basePackage("com.sinosoft.poc.icbca11.product.restapi"))	//通过指定扫描包暴露接口
                .paths(PathSelectors.any())	//设置过滤规则暴露接口
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("工银安盛A11项目测试")
                .description("")
                .version("1.0")
                .termsOfServiceUrl("")
                .license("")
                .licenseUrl("")
                .build();
    }
}
