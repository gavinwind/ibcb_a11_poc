/**
 * @Copyright ®2017 Sinosoft Co. Ltd. All rights reserved.
 * 项目名称 : 互联网核心系统项目
 * 创建日期 : 2017-11-13
 * 修改历史 :
 * 1. [2017-11-13]创建文件 by chenxin
 */
package com.sinosoft.poc.icbca11.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 【客户服务入口Application】
 * @author chenxin
 * @date 17/11/13 下午6:34 
 * @version V1.0
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.gavinwind.uic.customer.aop","com.gavinwind.uic.customer.biz" })
@EnableDiscoveryClient
@EnableTransactionManagement
@EnableFeignClients
@MapperScan("com.gavinwind.uic.customer.generated.mapper")
//@ImportResource (value={"applicationContext.xml" })
public class PocProductApplication {
    /**
     * 启动main方法
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(PocProductApplication.class,args);
    }
}
