/**
 * @Copyright ®2015 Sinosoft Co. Ltd. All rights reserved.
 * 项目名称 : 统一支付平台
 * 创建日期 : 2017-09-13
 * 修改历史 :
 * 1. [2017-09-13]创建文件 by chenxin
 */
package com.sinosoft.poc.icbca11.product.restapi;

import com.gavinwind.uic.customer.dto.CustomerRegParam;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 【客户操作Facade服务接口实现类】
 * @author chenxin
 * @date 2017-09-13 下午2:42 
 * @version V1.0
 */
@RestController
@RefreshScope
@RequestMapping(value="/uic/customerOper")
public class PocProductRestController{

    /**
     * 注册新客户(快捷方法)
     * @param regCustomer
     * @return
     */
    @ApiOperation(value="注册新用户", notes="通过客户简要信息注册新用户,如果发现用户名已经存在则抛出异常")
    @ApiImplicitParam(name = "regCustomer", value = "注册用户实体", required = true, dataType = "CustomerRegParam")
    @RequestMapping(value="register", method= RequestMethod.POST)
    public String registerNewCustomer(@RequestBody CustomerRegParam regCustomer) {
        return null;
    }

}
